function CyclesData = computeDrivingCycles(NavData, shouldPlotData)
%%
%   Computes data on driving cycles obtained from the steps generated per
%   route from the Google Maps API. Optionally plots data.
%
%   CyclesData = computeDrivingCycles(NavData, shouldPlotData)
%
    if nargin == 1
       shouldPlotData = false; 
    end
    
    assertNavData(NavData)
    
    if shouldPlotData
        text = ['Drive Cycles Data - ' NavData.id];
        hFigure = figure('Name', text, 'NumberTitle', 'off');
        set(hFigure, 'Position', get(0, 'Screensize'))
    end

    nRoutes = length(NavData.routes);
    for i = 1:nRoutes
        steps = NavData.routes(i).legs.steps;
        nSteps = length(steps);
        CyclesData.Routes(i).timeElapsed = zeros(1, nSteps);
        CyclesData.Routes(i).distanceCovered = zeros(1, nSteps);
        CyclesData.Routes(i).speeds = zeros(1, nSteps);

        for j = 1:nSteps
            step = steps{j};
            % accumulate time elapsed and distance covered since start of route
            if j ~= 1
                CyclesData.Routes(i).timeElapsed(j) = ...
                    CyclesData.Routes(i).timeElapsed(j-1) + step.duration.value;
                CyclesData.Routes(i).distanceCovered(j) = ...
                    CyclesData.Routes(i).distanceCovered(j-1) + step.distance.value;
            else
                CyclesData.Routes(i).timeElapsed(j) = step.duration.value;
                CyclesData.Routes(i).distanceCovered(j) = step.distance.value;
            end
            CyclesData.Routes(i).speeds(j) = step.distance.value/step.duration.value;
        end

        if shouldPlotData
            routeLabel = char('A'-1+i);
            
            % plot average speed to distance covered
            subplot(2, nRoutes, i)
            plot(CyclesData.Routes(i).distanceCovered, CyclesData.Routes(i).speeds)
            xlabel('distance covered (m)')
            ylabel('speed (m/s)')
            title(['Average speed per step over distance for route ' routeLabel])

            % plot average speed to time - nearly same as above
            subplot(2, nRoutes, i + nRoutes)
            plot(diff(CyclesData.Routes(i).timeElapsed))
            xlabel('steps')
            ylabel('time elapsed (s)')
            title(['Duration of each step for route ' routeLabel])
        end
    end
end
