# Internship at IITKGP #

## Toolboxes needed
1. Mapping Toolbox

## Step 1 - Complete
- Install MATLAB
- Make new Google ID (uaywp3) for accessing Elevation and Directions API from Google Maps
- Perform extensive patent searches and summarise results based on any fuel efficiency or route or time efficiency applications which have been developed with Google's APIs, and similar efforts from Google itself.
- Use HTTP URL based queries to test input coordinates for elevation and direction output
- The open source version only provides the most optimal path solution
- BUT we're looking for all paths - this is NOT a paid service
- Gather datasets in MATLAB for given path data.

## Step 2 - Complete
- We need example code to demonstrate the functions
- Get static map image data and place locations of step start and end points, or place these on a dynamic map.
- Verify grade data by evaluating if the steps are straight segments or by confirming the calculated displacement between start and end locations and the given distance.

## Step 3 - Complete
- Compute Drive Cycle data by studying and processing existing information.
- Add departure_time and traffic_model to Direction API request for traffic based data
- Use encoded polylines for obtaining elevations

## Step 4 - Complete
- Attempt to improve the current MATLAB functions with input validation
- Explore the use of polylines for location queries

## Step 5 - Complete
- Fix computeRouteGrades and computeDrivingCycles from these changes.

## Step 6 - Complete
- Create GUI
- **DO NOT OPEN gui.fig!** Run **gui** from console using `run gui`.