function [NavData, CyclesData, ElevationData, GradeData] = generateData(varargin)
%%
% generateData Run all Google Maps API requests and generate data from the
% responses.
%
%   generateData('id', STR, ...) retrieves data for existing Directions API
%   response.
%
%   generateData('origin', STR, 'destination', STR, ...) OR
%   generateData('origin', X, 'destination', Y, ...) makes a new Directions
%   API request with the given parameters.
%
%   EXAMPLE CALLS WITH PARAMETERS
%
%   [NavData, CyclesData, ElevationData, GradeData] = 
%   generateData('origin', 'kolkata wb', ...
%                'destination', 'iit kharagpur wb', ...
%                'showCycles', true, ... 
%                'showGrades', false, ...
%                'showMap', true, ...
%                'mapType', 'roadmap', ...
%                'dateTime, 'now')
%
%   generateData('origin', [28.7335344 88.3778901], ...
%                'destination', '22.5728456,77.1025762', ...
%                'mapType', 'roadmap')
%
%   generateData('id', '13Jun2018_11-17-19-007', ...
%                'showMap', true)
%
%   NOTE
%
%   - 'forceNewMapRequest' and 'forceNewElevationsRequests' is mostly
%   unnecessary, since an image or the elevation data for routes is not
%   bound to differ for the same Directions API response.
%
%   - 'mapType' is ignored for calls with the 'id' parameter unless
%   'forceNewMapRequest' is set to true.
%   
%   - 'saveAllToMatFile' saves all generated data to a .mat file with the
%   given file ID.
%
    global Strings;
    if isempty(Strings)
        Strings = jsondecode(fileread('strings.json'));
    end
    
    % validate function inputs
    inputs = inputParser;
    inputs.StructExpand = false;
    
    addParameter(inputs, 'id', '', @validateNotEmpty)
    addParameter(inputs, 'origin', '', @validateNotEmpty)
    addParameter(inputs, 'destination', '', @validateNotEmpty)
    addParameter(inputs, 'showCycles', true, @islogical)
    addParameter(inputs, 'showGrades', true, @islogical)
    addParameter(inputs, 'showMap', true, @islogical)
    addParameter(inputs, 'mapType', 'roadmap', @isMapsStaticType)
    addParameter(inputs, 'dateTime', '', @validateNotEmpty)
    addParameter(inputs, 'forceNewMapRequest', false, @islogical)
    addParameter(inputs, 'forceNewElevationsRequests', false, @islogical)
    addParameter(inputs, 'saveAllToMatFile', false, @islogical)
    
    parse(inputs, varargin{:});
    
    id = inputs.Results.id;
    origin = inputs.Results.origin;
    destination = inputs.Results.destination;
    showCycles = inputs.Results.showCycles;
    showGrades = inputs.Results.showGrades;
    showMap = inputs.Results.showMap;
    mapType = inputs.Results.mapType;
    dateTime = inputs.Results.dateTime;
    forceNewMapRequest = inputs.Results.forceNewMapRequest;
    forceNewElevationsRequests = inputs.Results.forceNewElevationsRequests;
    saveAllToMatFile = inputs.Results.saveAllToMatFile;
    
    % the 'id' and 'origin'/'destination' parameters should be mutually
    % exclusive.
    if ~isempty(id) && ~isempty(origin) && ~isempty(destination)
        warning('If an ID is specified, origin and destination are ignored.')
        NavData = readNavDataById(id)
    elseif ~isempty(id)
        NavData = readNavDataById(id)
    elseif ~isempty(origin) && ~isempty(destination)
        NavData = fetchNavigationData(origin, destination, dateTime)
    else % all fields were left blank, so that's a bummer
        warning([
            'Please provide an ID, or origin and destination locations. ' ...
            'Check documentation for more details.'
            ]);
        error('Missing file ID or origin and destination parameters.')
    end
    
    CyclesData = computeDrivingCycles(NavData, showCycles)
    [ElevationData, GradeData] = computeRouteGrades(NavData, forceNewElevationsRequests, showGrades)
    savedImagePath = fetchStaticMap(NavData, mapType, forceNewMapRequest, showMap)
    
    if saveAllToMatFile
        path = fullfile(pwd, Strings.Directories.saved, [NavData.id '.mat']);
        file = matfile(path, 'Writable', true)
        file.NavData = NavData
        file.CyclesData = CyclesData
        file.ElevationData = ElevationData
        file.GradeData = GradeData
        clear('file')
    end
end

function TF = validateNotEmpty(x)
%%
%   Produces an error for empty arrays or structs.
%
    TF = ~(isstruct(x) && isempty(fieldnames(x))) || isempty(x);
    
    assert(TF, 'Parameter cannot be empty!')
end

function TF = isMapsStaticType(mapType)
%%
%   Checks if it's a Maps Static API compatible map type.
%
    global Strings
    TF = any(strcmp(Strings.MapsStaticTypes, mapType));
    
    assert(TF, [
        'Invalid maptype parameter! Must be one of ' ...
        join(string(Strings.MapsStaticTypes), ', ') ...
        ]);
end

function TF = isDateTime(dateTime)
%%
%   Checks if it's a Maps Static API compatible map type.
%
    TF = true;
    
    try
        datetime(dateTime)
    catch err
        error(err.identifier, [
            'Invalid date and time! ' ...
            'Use a date that looks like 13-Mar-2019 04:36:23! ' ...
            'You may also use ''today'', ''tomorrow'', or ''now'', without quotes.'
            ])
        TF = false;
    end
end

