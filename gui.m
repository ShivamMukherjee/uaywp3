function varargout = gui(varargin)
% GUI MATLAB code for gui.fig
%      GUI, by itself, creates a new GUI or raises the existing
%      singleton*.
%
%      H = GUI returns the handle to a new GUI or the handle to
%      the existing singleton*.
%
%      GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GUI.M with the given input arguments.
%
%      GUI('Property','Value',...) creates a new GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gui

% Last Modified by GUIDE v2.5 04-Jul-2018 21:44:05

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gui_OpeningFcn, ...
                   'gui_OutputFcn',  @gui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

    global Strings
    if isempty(Strings)
        Strings = jsondecode(fileread('strings.json'));
    end
end

% --- Executes just before gui is made visible.
function gui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gui (see VARARGIN)

% Choose default command line output for gui
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes gui wait for user response (see UIRESUME)
% uiwait(handles.figure1);
end

% --- Outputs from this function are returned to the command line.
function varargout = gui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;
end


function id_Callback(hObject, eventdata, handles)
% hObject    handle to id (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of id as text
%        str2double(get(hObject,'String')) returns contents of id as a double

    % If assigned, origin and destination aren't being used
    set(handles.origin, 'String', '')
    set(handles.destination, 'String', '')
end


% --- Executes during object creation, after setting all properties.
function id_CreateFcn(hObject, eventdata, handles)
% hObject    handle to id (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end


function origin_Callback(hObject, eventdata, handles)
% hObject    handle to origin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of origin as text
%        str2double(get(hObject,'String')) returns contents of origin as a double

    % If assigned, ID isn't being used
    set(handles.id, 'String', '')
end

function destination_Callback(hObject, eventdata, handles)
% hObject    handle to destination (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of destination as text
%        str2double(get(hObject,'String')) returns contents of destination as a double
    
    % If assigned, ID isn't being used
    set(handles.id, 'String', '')
end


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over run.
function run_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to run (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    
    id = get(handles.id, 'String');
    origin = get(handles.origin, 'String');
    destination = get(handles.destination, 'String');
    mapType = get(handles.mapType, 'String');
    dateTime = get(handles.dateTime, 'String');
    showCycles = logical(get(handles.showCycles, 'Value'));
    showGrades = logical(get(handles.showGrades, 'Value'));
    showMap = logical(get(handles.showMap, 'Value'));
    forceNewMapRequest = logical(get(handles.forceNewMapRequest, 'Value'));
    forceNewElevationsRequests = logical(get(handles.forceNewElevationsRequests, 'Value'));
    saveAllToMatFile = logical(get(handles.saveAllToMatFile, 'Value'));
    
    % Reassurance to the user!
    log(handles, [id ': Running...'])
    
    try
        [NavData, ~, ~, ~] = generateData(...
                'id', id, 'origin', origin, 'destination', destination, ...
                'mapType', mapType, ...
                'dateTime', dateTime, ...
                'showCycles', showCycles, 'showGrades', showGrades, 'showMap', showMap, ...
                'forceNewMapRequest', forceNewMapRequest, ...
                'forceNewElevationsRequests', forceNewElevationsRequests, ...
                'saveAllToMatFile', saveAllToMatFile ...
        );
    
        if saveAllToMatFile
            log(handles, [id ': Done and saved as ' NavData.id '.mat!'])
        else
            log(handles, [id ': Done!'])
        end
        
    catch error
        log(handles, error)
    end

    % update list of saved responses again
    set(handles.fileIdList, 'String', findAllResponses())
end


% --- Executes during object creation, after setting all properties.
function origin_CreateFcn(hObject, eventdata, handles)
% hObject    handle to origin (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

% --- Executes during object creation, after setting all properties.
function destination_CreateFcn(hObject, eventdata, handles)
% hObject    handle to destination (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
end

% --- Executes during object creation, after setting all properties.
function mapType_CreateFcn(hObject, eventdata, handles)
% hObject    handle to mapType (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end
    
    set(hObject, 'String', 'roadmap')
end

% --- Executes during object creation, after setting all properties.
function showCycles_CreateFcn(hObject, eventdata, handles)
% hObject    handle to showCycles (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
    set(hObject, 'Value', true)
end

% --- Executes during object creation, after setting all properties.
function showGrades_CreateFcn(hObject, eventdata, handles)
% hObject    handle to showGrades (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
    set(hObject, 'Value', true)
end

% --- Executes during object creation, after setting all properties.
function showMap_CreateFcn(hObject, eventdata, handles)
% hObject    handle to showMap (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
    set(hObject, 'Value', true)
end

% --- Executes during object creation, after setting all properties.
function forceNewMapRequest_CreateFcn(hObject, eventdata, handles)
% hObject    handle to forceNewMapRequest (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
    set(hObject, 'Value', false)
end

% --- Executes during object creation, after setting all properties.
function forceNewElevationsRequests_CreateFcn(hObject, eventdata, handles)
% hObject    handle to forceNewElevationsRequests (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
    set(hObject, 'Value', false)
end


% --- Executes on selection change in fileIdList.
function fileIdList_Callback(hObject, eventdata, handles)
% hObject    handle to fileIdList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns fileIdList contents as cell array
%        contents{get(hObject,'Value')} returns selected item from fileIdList
    
    global messageText

    % Updates the ID field from the current selection in the pop-up menu.
    index = get(hObject, 'Value');
    names = get(hObject, 'String');

    set(handles.id, 'String', names{index})
end

% --- Executes during object creation, after setting all properties.
function fileIdList_CreateFcn(hObject, eventdata, handles)
% hObject    handle to fileIdList (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
    
    % Populate list with existing NavData response names
    set(hObject, 'String', findAllResponses())
end


% --- Executes during object creation, after setting all properties.
function saveAllToMatFile_CreateFcn(hObject, eventdata, handles)
% hObject    handle to saveAllToMatFile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
    set(hObject, 'Value', false)
end



% ---  Helper functions    %

function names = findAllResponses()
%%
%   Retrieve all NavData JSON responses in SavedResponses.
%
   
    global Strings
    
    responses = dir(fullfile(pwd, Strings.Directories.saved, '*.json'));
    names = cell(1, length(responses));
    [names{:}] = responses.name;
    names = erase(names, '.json');
end

function log(handles, obj, category)
%%
%   Logs to the Messages box in the window. Only works after all
%   CreateFcns have been called; if used in CreateFcns, store the message
%   and display it from the respective Callback function.
%
    % maintain a char vector for storing logs, and store it in a MAT file
    logFile = matfile('log', 'Writable', true);

    % handle the usage of MExceptions
    if isa(obj, 'MException')
        category = 'error';
    elseif nargin == 2 && (ischar(obj) || istring(obj))
        category = '';
    end
    
    % get the message and the first line in the call stack, if applicable
    obj = errorHead(obj);
    
    % red means bad things!
    switch category
        case 'error'
            set(handles.messages, 'ForegroundColor', 'red')
        otherwise
            set(handles.messages, 'ForegroundColor', 'black')
    end
    
    % store the log and show it
    logFile.log = [logFile.log; string(obj)];
    clear('logFile');
    set(handles.messages, 'String', obj)
end

function str = errorHead(error)
%%
%   Get the message and the first line of its call stack, if available.
%
    if isa(error, 'MException')
        if isfield(error, 'stack') && ~isempty(error.stack)
            str = [error.message ' (' [error.stack(1).file ' at line ' num2str(error.stack(1).line)] ')'];
        else
            str = error.message;
        end
    else
        str = char(error);
    end
end

% 
% function message = simplifyError(error)
% %%
% %   Simplifies log messages for very specific errors.
% %
%     switch error.identifier
%         case 'MATLAB:webservices:CopyContentToDataStreamError'
%             message = 'You aren''t connected to the Internet!';
%         otherwise
%             message = error.message;
%     end
% end


% --- Executes during object creation, after setting all properties.
function dateTime_CreateFcn(hObject, eventdata, handles)
% hObject    handle to dateTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
    set(hObject, 'String', 'now');
end
